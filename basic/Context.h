#ifndef ARKUI_CONTEXT_H
#define ARKUI_CONTEXT_H
#include "Style.h"

namespace arke::ui {
    typedef uint32_t entity_t;
    
    class Context {
    public:
        typedef uint8_t Type;
        static constexpr const Type 
            Invalid = 0,
            TUI = 1,
            Web = 2,
            GL = 3;
            
        
        virtual ~Context();
        
        virtual bool initialize() = 0;
        virtual void terminate() = 0;
        
        virtual void write(const uivec& pos, const std::string& str, const Style& style = Style()) = 0;
        virtual void writew(const uivec& pos, const char *str, const Style& style = Style()) = 0;
        virtual void writew(const uivec& pos, const wchar_t *str, const Style& style = Style()) = 0;
        
        virtual void box(const uivec& ul, const uivec& lr, const Style& style = Style()) = 0;
        virtual void line(uivec s, uivec e, const Style& style = Style()) = 0;
        
        virtual void image(const uivec& ul, const uivec& sz, const Style& style = Style()) = 0;
        
        virtual void entity(const uivec& pos, entity_t enty, const Style& style = Style()) = 0;
        
        virtual void update() = 0;
        
        virtual Type type() const = 0;
        
        virtual uivec size() const;
        virtual void push(uivec offset, uivec sz);
        virtual void pop();
    protected:
        struct {
            uivec offset, size;
        } current;
    };
}

#endif
