#ifndef ARKUI_UPDATABLE_H
#define ARKUI_UPDATABLE_H

namespace arke::ui {
    class Updatable {
    public:
        inline virtual ~Updatable() {};
        
        virtual void update() = 0;
    protected:
        
    }
}
