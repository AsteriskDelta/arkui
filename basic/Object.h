#ifndef ARKUI_OBJECT_H
#define ARKUI_OBJECT_H
#include "Style.h"

namespace arke::ui {
    class Object : public virtual Proxy {
    public:
        Object();
        virtual ~Object();
        
        uivec position, size;
        Style style;
        
        virtual void draw();
        virtual void resize(unsigned int x, unsigned int y, bool preserve = false);
        
        inline virtual void setDirty() {
            objFlags.dirty = true;
        }
        inline virtual void clearDirty() {
            objFlags.dirty = false;
        }
        inline virtual bool dirty() const {
            return objFlags.dirty;
        }
    protected:
        uivec absolute() const;
        Object* parent() const;
        
        struct {
            bool dirty;
        } objFlags;
    };
}

#endif
