#include "CSVStream.h"

namespace GDAX {
    
    
    CSVStream::CSVStream() : Stream() {
        
    }
    CSVStream::~CSVStream() {
        
    }
    
    bool CSVStream::connected() const {
        return csv.good();//is_open();
    }
    
    bool CSVStream::connect(const std::string& path) {
        csv.open(path, std::iostream::in);
        
        return this->connected();
    }
    
    void CSVStream::disconnect() {
        if(csv.is_open()) csv.close();
    }
    
    std::string CSVStream::getLine() {
        std::string ret;
        //Skip empty lines
        while((std::getline(csv, ret), ret).empty() && !csv.eof()) { };
        
        return ret;
    }
    
};
