#ifndef ARKUI_FOCUSABLE_H
#define ARKUI_FOCUSABLE_H

namespace arke::ui {
    class Focusable {
    public:
        inline virtual ~Focusable() {};
        
        inline virtual void onFocus() {};
        inline virtual void onDefocus() {}};
    protected:
        
    }
}

#endif
