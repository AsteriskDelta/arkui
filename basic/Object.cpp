#include "Object.h"

namespace arke::ui {
    Object::Object() : position{0,0}, size{0,0}, style() {
        
    }
    
    Object::~Object() {
        
    }
    
    void Object::draw() {
        for(auto it = this->begin<Object>(); it != this->end<Object>(); ++it) {
            it->draw();
        }
    }
    
    void Object::resize(unsigned int x, unsigned int y, bool preserve) {
        this->size.x = x;
        this->size.y = y;
        _unused(preserve);
    }
    
    Object* Object::parent() const {
        if(Proxy::parent() == nullptr) return nullptr;
        else return dynamic_cast<Object*>(Proxy::parent());
    }
    
    uivec Object::absolute() const {
        //std::cerr << "object at " << this->position << " has parent=" << this->parent() << " from proxy ptr " << Proxy::parent() << " with " << Proxy::_children.size() << " children\n";
        if(this->parent() != nullptr) return this->parent()->absolute() + this->position;
        else return this->position;
    }
}
