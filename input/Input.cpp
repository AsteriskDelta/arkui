#include "Input.h"
#include "Context.h"
#include "InputID.h"
#include <ncurses.h>

namespace arke::ui {
    
    namespace Input {
        typedef int tick_t;
        tick_t Tick = 0;
    
        struct RawKey {
            tick_t tickDown = -1, tickUp = -1;
            inline void onDown(int d = 0) {
                tickDown = Tick + d;
            }
            inline void onUp(int d = 0) {
                tickUp = Tick + d;
            }
            
            inline bool down() const {
                return tickDown != -1 && tickDown < Tick && (tickUp == -1 || tickUp >= Tick);
            }
            inline bool up() const {
                return tickUp != -1 && tickUp < Tick && (tickDown == -1 || tickDown > Tick);
            }
        };
        std::unordered_map<InputID, RawKey> Raws;
        
        
        void Update(Context *ctx) {
            switch(ctx->type()) {
                default:
                case Context::TUI: {
                    int ch;
                    while((ch = getch()) != ERR) {
                        InputID id = InputID(ch);
                        Raws[id].onDown();
                        Raws[id].onUp(1);
                    }
                    break;}
            };
            
            Tick++;
        }
        
        bool GetKeyDown(char c) {
            InputID id(c);
            return Raws[id].down();
        }
        bool GetKeyDown(InputID c) {
            return Raws[c].down();
        }
    }
}
