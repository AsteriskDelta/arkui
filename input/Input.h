#ifndef ARKUI_INPUT_H
#define ARKUI_INPUT_H
#include "InputID.h"

namespace arke::ui {
    namespace Input {
        void Update(Context *ctx);
        
        bool GetKeyDown(char c);
        bool GetKeyDown(InputID c);
    }
}

#endif
