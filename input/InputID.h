#ifndef ARKUI_INPUTID_H
#define ARKUI_INPUTID_H

namespace arke::ui {
    class InputID {
    public:
        inline InputID() : code(0x0) {};
        //inline InputID(char c) : code(c) {};
        inline InputID(int32_t c) : code(c){};
        uint32_t code;
        inline bool operator==(const InputID& o) const {
            return this->code == o.code;
        }
        inline bool operator!=(const InputID& o) const {
            return !(*this == o);
        }
    };
};

namespace std {
    template <>
    struct hash<arke::ui::InputID> {
        size_t operator()( const arke::ui::InputID& k ) const {
            size_t res = k.code;
            return res;
        }
    };
};

#endif
