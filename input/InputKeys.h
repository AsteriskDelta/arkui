#pragma once
#include <iostream>
#include <string>
#include "rWindow.h"

std::string Input::keyNames[INPUT_MAX_KEYS];
inline void Input::LoadKeyNames() { _prsguard();
  keyNames[0]				   = "Error [Range]";
  keyNames[STK( SDLK_BACKSPACE		)] = "Backspace";
  keyNames[STK( SDLK_TAB		)] = "Tab";
  keyNames[STK( SDLK_CLEAR		)] = "Clear";
  keyNames[STK( SDLK_RETURN		)] = "Return";
  keyNames[STK( SDLK_PAUSE		)] = "Pause";
  keyNames[STK( SDLK_ESCAPE		)] = "Escape";
  keyNames[STK( SDLK_SPACE		)] = "Spacebar";
  keyNames[STK( SDLK_DELETE		)] = "Delete";
  
  keyNames[STK( SDLK_KP_0		)] = "Keypad 0";
  keyNames[STK( SDLK_KP_1		)] = "Keypad 1";
  keyNames[STK( SDLK_KP_2		)] = "Keypad 2";
  keyNames[STK( SDLK_KP_3		)] = "Keypad 3";
  keyNames[STK( SDLK_KP_4		)] = "Keypad 4";
  keyNames[STK( SDLK_KP_5		)] = "Keypad 5";
  keyNames[STK( SDLK_KP_6		)] = "Keypad 6";
  keyNames[STK( SDLK_KP_7		)] = "Keypad 7";
  keyNames[STK( SDLK_KP_8		)] = "Keypad 8";
  keyNames[STK( SDLK_KP_9		)] = "Keypad 9";
  
  keyNames[STK( SDLK_KP_PERIOD		)] = "Keypad .";
  keyNames[STK( SDLK_KP_DIVIDE		)] = "Keypad /";
  keyNames[STK( SDLK_KP_MULTIPLY	)] = "Keypad *";
  keyNames[STK( SDLK_KP_MINUS		)] = "Keypad -";
  keyNames[STK( SDLK_KP_PLUS		)] = "Keypad +";
  keyNames[STK( SDLK_KP_ENTER		)] = "Keypad \\R";
  keyNames[STK( SDLK_KP_EQUALS		)] = "Keypad =";
  
  keyNames[STK( SDLK_UP			)] = "Up Arrow";
  keyNames[STK( SDLK_DOWN		)] = "Down Arrow";
  keyNames[STK( SDLK_LEFT		)] = "Left Arrow";
  keyNames[STK( SDLK_RIGHT		)] = "Right Arrow";
  
  keyNames[STK( SDLK_INSERT		)] = "Insert";
  keyNames[STK( SDLK_HOME		)] = "Home";
  keyNames[STK( SDLK_END		)] = "End";
  
  keyNames[STK( SDLK_PAGEUP		)] = "Page Up";
  keyNames[STK( SDLK_PAGEDOWN		)] = "Page Down";
  
  keyNames[STK( SDLK_F1			)] = "F1";
  keyNames[STK( SDLK_F2			)] = "F2";
  keyNames[STK( SDLK_F3			)] = "F3";
  keyNames[STK( SDLK_F4			)] = "F4";
  keyNames[STK( SDLK_F5			)] = "F5";
  keyNames[STK( SDLK_F6			)] = "F6";
  keyNames[STK( SDLK_F7			)] = "F7";
  keyNames[STK( SDLK_F8			)] = "F8";
  keyNames[STK( SDLK_F9			)] = "F9";
  keyNames[STK( SDLK_F10		)] = "F10";
  keyNames[STK( SDLK_F11		)] = "F11";
  keyNames[STK( SDLK_F12		)] = "F12";
  keyNames[STK( SDLK_F13		)] = "F13";
  keyNames[STK( SDLK_F14		)] = "F14";
  keyNames[STK( SDLK_F15		)] = "F15";
  
  keyNames[STK( SDLK_CAPSLOCK		)] = "Caps Lock";
  keyNames[STK( SDLK_SCROLLLOCK		)] = "Scroll Lock";
  
  keyNames[STK( SDLK_RSHIFT		)] = "Right Shift";
  keyNames[STK( SDLK_LSHIFT		)] = "Left Shift";
  keyNames[STK( SDLK_RCTRL		)] = "Right Ctrl";
  keyNames[STK( SDLK_LCTRL		)] = "Left Ctrl";
  keyNames[STK( SDLK_RALT		)] = "Right Alt";
  keyNames[STK( SDLK_LALT		)] = "Left Alt";
  
  keyNames[STK( SDLK_PRINTSCREEN	)] = "Print Sc";
  
  /*
  keyNames[STK( SDLK_EXCLAIM		)] = "!";
  
  keyNames[STK( SDLK_QUOTEDBL		)] = "\"";
  keyNames[STK( SDLK_HASH		)] = "#";
  keyNames[STK( SDLK_DOLLAR		)] = "$";
  keyNames[STK( SDLK_AMPERSAND		)] = "&";
  keyNames[STK( SDLK_QUOTE		)] = "'";
  keyNames[STK( SDLK_LEFTPAREN		)] = "(";
  keyNames[STK( SDLK_RIGHTPAREN		)] = ")";
  keyNames[STK( SDLK_ASTERISK		)] = "*";
  */
  
  keyNames[MPK(1			)] = "Left Mouse";
  keyNames[MPK(2			)] = "Middle Mouse";
  keyNames[MPK(3			)] = "Right Mouse";
  keyNames[MPK(5			)] = "Wheel Down";
  keyNames[MPK(4			)] = "Wheel Up";
  keyNames[MPK(6			)] = "Wheel Left";
  keyNames[MPK(7			)] = "Wheel Right";
  keyNames[MPK(8			)] = "Mouse 8";
  keyNames[MPK(9			)] = "Mouse 9";
  keyNames[MPK(10			)] = "Mouse 10";
  keyNames[MPK(11			)] = "Mouse 11";
  keyNames[MPK(12			)] = "Mouse 12";
  
  keyNames[CPK(SDL_CONTROLLER_BUTTON_A		)] = "Ct A Button";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_B		)] = "Ct B Button";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_X		)] = "Ct X Button";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_Y		)] = "Ct Y Button";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_BACK	)] = "Ct Back";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_GUIDE	)] = "Ct Guide";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_START	)] = "Ct Start";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_LEFTSTICK	)] = "Ct L Stick";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_RIGHTSTICK	)] = "Ct R Stick";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_LEFTSHOULDER	)] = "Ct L Shoulder";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_RIGHTSHOULDER	)] = "Ct R Shoulder";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_DPAD_UP	)] = "Ct Up";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_DPAD_DOWN	)] = "Ct Down";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_DPAD_LEFT	)] = "Ct Left";
  keyNames[CPK(SDL_CONTROLLER_BUTTON_DPAD_RIGHT	)] = "Ct Right";
}
