#ifndef INPUT_OBJECT_H
#define INPUT_OBJECT_H
#include "iARKUI.h"

namespace arkui {
    namespace Input {
        class Object {
        public:
            typedef unsigned int Idt_t
            
            Object(const std::string&& nam, const std::string&& short);
            virtual ~Object();
            
            Idt_t id;
            std::string name, shortName;
        protected:
            static Idt_t Count;
        };
        
    };
    
};

#endif

