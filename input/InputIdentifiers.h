#ifndef INPUT_IDENS_H
#define INPUT_IDENS_H

#include "InputID.h"

//#ifdef INPUT_IDENS_CPP
#define UIOO_DECL(NAME,SHORT,TYPE)
#define UIOM_DECL(...)
#define UIOL_DECL(...)
#define UIOS_DECL(NAME, TYPE)
//#else
//#define UIO_DECL(NAME,SHORT,TYPE)
//#endif

namespace arke::ui {
    namespace Input {
        class Object;
        class Key;
        class Axis;
        class Pointer;
        
        namespace Keys {
            UIOO_DECL("Right Control"    , RightControl      , InputKey);
            UIOO_DECL("Left Control"     , LeftControl       , InputKey);
            UIOM_DECL(Control, RightControl, LeftControl);
            
            UIOO_DECL("Right Shift"      , RightShift       , InputKey);
            UIOO_DECL("Left Shift"       , LeftShift        , InputKey);
            UIOM_DECL(Shift, RightShift, LeftShift);
            
            UIOO_DECL("Right Alt"        , RightAlt         , InputKey);
            UIOO_DECL("Left Alt"         , LeftAlt          , InputKey);
            UIOM_DECL(Alt, RightAlt, LeftALt);
            
            UIOO_DECL(","               , System              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            
            
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            
            
            UIOO_DECL("Escape"          , Escape            , InputKey);
            UIOO_DECL("Insert"          , Insert            , InputKey);
            UIOO_DECL("Delete"          , Delete            , InputKey);
            UIOO_DECL("Pause"           , Pause             , InputKey);
            
            
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            UIOO_DECL(","               , Plus              , InputKey);
            
            UIOL_DECL(A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z);
            
            UIOO_DECL("`"               , Backtick          , InputKey);
            UIOO_DECL("1"               , _1                , InputKey);
            UIOO_DECL("2"               , _2                , InputKey);
            UIOO_DECL("3"               , _3                , InputKey);
            UIOO_DECL("4"               , _4                , InputKey);
            UIOO_DECL("5"               , _5                , InputKey);
            UIOO_DECL("6"               , _6                , InputKey);
            UIOO_DECL("7"               , _7                , InputKey);
            UIOO_DECL("8"               , _8                , InputKey);
            UIOO_DECL("9"               , _9                , InputKey);
            UIOO_DECL("0"               , _0                , InputKey);
            UIOO_DECL("-"               , Minus             , InputKey);
            UIOO_DECL("="               , Equals            , InputKey);
            
            UIOO_DECL("~"               , Tilde             , InputKey);
            UIOO_DECL("!"               , Exclame           , InputKey);
            UIOO_DECL("@"               , At                , InputKey);
            UIOO_DECL("#"               , Pound             , InputKey);
            UIOO_DECL("%"               , Percent           , InputKey);
            UIOO_DECL("^"               , Carrot            , InputKey);
            UIOO_DECL("&"               , Ampersand         , InputKey);
            UIOO_DECL("*"               , Asterisk          , InputKey);
            UIOO_DECL("("               , LeftParen         , InputKey);
            UIOO_DECL(")"               , RightParen        , InputKey);
            UIOO_DECL("_"               , Underscore        , InputKey);
            UIOO_DECL("+"               , Plus              , InputKey);
            
            
            
            UIOO_DECL(","               , Comma              , InputKey);
            UIOO_DECL("."               , Period              , InputKey);
            UIOO_DECL("/"               , Slash              , InputKey);
            UIOO_DECL("<"               , LeftAngle              , InputKey);
            UIOO_DECL(">"               , RightAngle              , InputKey);
            UIOO_DECL("?"               , Question              , InputKey);
            
            UIOO_DECL(";"               , Semicolon             , InputKey);
            UIOO_DECL(":"               , Colon              , InputKey);
            UIOO_DECL("'"               , Tick            , InputKey);
            UIOO_DECL("\""              , Quote              , InputKey);
            
            UIOO_DECL("["               , LeftBracket              , InputKey);
            UIOO_DECL("]"               , RightBracket              , InputKey);
            UIOO_DECL("\\"              , Backlash              , InputKey);
            UIOO_DECL("{"               , LeftBrace              , InputKey);
            UIOO_DECL("}"               , RightBrace              , InputKey);
            UIOO_DECL("|"               , Pipe              , InputKey);
            
            UIOS_DECL(Up, InputKey);
            UIOS_DECL(Down, InputKey);
            UIOS_DECL(Left, InputKey);
            UIOS_DECL(Right, InputKey);
            
            
            UIOS_DECL(F1, InputKey);
            UIOS_DECL(F2, InputKey);
            UIOS_DECL(F3, InputKey);
            UIOS_DECL(F4, InputKey);
            UIOS_DECL(F5, InputKey);
            UIOS_DECL(F6, InputKey);
            UIOS_DECL(F7, InputKey);
            UIOS_DECL(F8, InputKey);
            UIOS_DECL(F9, InputKey);
            UIOS_DECL(F10, InputKey);
            UIOS_DECL(F11, InputKey);
            UIOS_DECL(F12, InputKey);
            
            UIOS_DECL(Keypad1, InputKey);
            UIOS_DECL(Keypad2, InputKey);
            UIOS_DECL(Keypad3, InputKey);
            UIOS_DECL(Keypad4, InputKey);
            UIOS_DECL(Keypad5, InputKey);
            UIOS_DECL(Keypad6, InputKey);
            UIOS_DECL(Keypad7, InputKey);
            UIOS_DECL(Keypad8, InputKey);
            UIOS_DECL(Keypad9, InputKey);
            UIOS_DECL(Keypad0, InputKey);
        }
        
        
        const std::string& NameOf(const InputID&& id);
        
        void AddName(const InputID&& id, const std::string&& str);
        
        void InitializeIdentifiers();
    };
};

#endif

