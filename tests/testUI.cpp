#include "ARKUI.h"
#include "front-curses/Curses.h"
#include <ncurses.h>
#include "Style.h"
#include "Label.h"
#include "Frame.h"
#include "Bar.h"
#include "Input.h"
using namespace arke;

int main(int argc, char** argv) {
    ui::Context *ctx = new ui::CursesObject();
    ui::ActiveContext = ctx;
    ctx->initialize();
    
    ui::Style a, b,c;
    
    a.color.fg = ColorRGBA(255,0,0);
    b.color.fg = ColorRGBA(0, 255, 0);
    c.color.fg = ColorRGBA(0, 0, 255);
    
    a.color.bg = ColorRGBA(0,0,0);
    b.color.bg = ColorRGBA(255,255,0);
    c.color.bg = ColorRGBA(255,0,0);
    
    a.set(ui::Style::Bold);
    b.set(ui::Style::Bold);
    c.set(ui::Style::Bold);
    
    ctx->box({5,5}, {20,20}, a);
    ctx->write({8,12}, "it works!!!", a);
    
    ctx->write({8,13}, "second!!", b);
    ctx->write({8,14}, "third!!", c);
    
    ui::Frame *frame = new ui::Frame();
    frame->title = "test frame!";
    frame->position = uivec{40,5};
    frame->size = uivec{30,15};
    frame->style = a;
    
    ui::Label *label = new ui::Label();
    label->position = uivec{5,5};
    label->size = uivec{25, 1};
    label->style = a;
    label->data = std::string("inside a labelϪ");
    
    //label->draw();
    frame->add(label);
    frame->draw();
    
    ui::Bar bar;
    bar.position = uivec{10, 30};
    bar.size = uivec{30, 0};
    bar.style = a;
    bar.setLeftText("HP");
    bar.setRightText("-->");
    
    bar.draw();
    
    ctx->writew({30,10}, "背景：不白", a);
    
    ctx->writew({30,11}, "Ϫ Ͽ", a);
    
    ctx->update();
    
    float p = 0.0;
    while(true) {
        ui::Input::Update(ctx);
        
        for(int i = 32; i <= 127; i++) {
            if(ui::Input::GetKeyDown(char(i))) {
                label->data = "Pressed ";
                label->data += char(i);
                label->draw();
            }
        }
        
        bar.setProgress(p);
        bar.draw();
        p += 0.0033;
        usleep(100000);
    }
    ctx->terminate();
    return 0;
}
