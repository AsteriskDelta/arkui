#ifndef ARKUI_CURSES_H
#define ARKUI_CURSES_H
#include "Context.h"

namespace arke::ui {
    class CursesObject : public Context {
    public:
        virtual ~CursesObject();
        
        virtual bool initialize() override;
        virtual void terminate() override;
        
        virtual void write(const uivec& pos, const std::string& str, const Style& style = Style()) override;
        virtual void writew(const uivec& pos, const char * str, const Style& style = Style()) override;
        virtual void writew(const uivec& pos, const wchar_t * str, const Style& style = Style()) override;
        
        
        virtual void box(const uivec& ul, const uivec& lr, const Style& style = Style()) override;
        virtual void line(uivec s, uivec e, const Style& style = Style()) override;
        
        virtual void image(const uivec& ul, const uivec& sz, const Style& style = Style()) override;
        
        virtual void entity(const uivec& pos, entity_t enty, const Style& style = Style()) override;
        
        virtual void update() override;
        
        virtual void applyStyle(const Style& style);
        
        inline virtual Context::Type type() const override {
            return Context::TUI;
        }
    protected:
        uivec size;
        
        int getStyleFlags(const Style& style);
        unsigned int getColor(ColorRGBA col);
        unsigned int getColorPair(ColorRGBA fg, ColorRGBA bg);
        
        ColorRGBA cfg, cbg;
    };
    
    
}

#endif
