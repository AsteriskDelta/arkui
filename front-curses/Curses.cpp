#include "Curses.h"
#include <ncurses.h>
#include <signal.h>
#include "Style.h"

namespace arke::ui {
    constexpr const int ColorGrades = 6;
    constexpr const int ColorGradeMult = ColorGrades-1;
    constexpr const int ColorGradeInc = 256;
    
    CursesObject::~CursesObject() {
        
    }
    
    void CursesResizeHandler(int sig) {
          _unused(sig);
    }
    
    bool CursesObject::initialize() {
        setlocale(LC_ALL, "");
        initscr();
        start_color();
        assume_default_colors(-1,-1);
        //raw();
        noecho();
        cbreak();
        keypad(stdscr, 1);
        nodelay(stdscr, TRUE);
        curs_set(0);
        signal(SIGWINCH, CursesResizeHandler);
        
        std::cerr <<  COLORS << " : " << COLOR_PAIRS << "\n";
        
        std::cerr << "Change colors: " << can_change_color();
        //sleep(3);
        
        //std::cout << "Plcount: " << color_pairs() << "\n";
        /*unsigned short f, b; // For foreground and background respectively.
        for ( f = 0; f < COLORS; ++f ) {
            for ( b = 0; b < COLORS; ++b )  {
                init_pair ( f * COLORS + b, f, b );
            }
        }*/
        
        //These are already the defaults, no need to set them ourselves :/
        //unsigned int ci = 16;
        for(unsigned int r = 0; r < ColorGrades; r++) {
            for(unsigned int g = 0; g < ColorGrades; g++) {
                for(unsigned int b = 0; b < ColorGrades; b++) {
                    ColorRGB col;
                    col.r = r * 255 / ColorGradeMult;
                    col.g = g * 255 / ColorGradeMult;
                    col.b = b * 255 / ColorGradeMult;
                    unsigned int colID = b + g*ColorGrades + r * (ColorGrades*ColorGrades) + 16;
                    init_extended_color(colID,  int(col.r) * 200 / 51,
                                    int(col.g) * 200 / 51, 
                                    int(col.b) * 200 / 51);
                    //std::cout << "set color " << ci << " raw " << col.toString() <<" to " << (int(col.r) * 200 / 51) << ", " << (int(col.g) * 200 / 51) << ", " << (int(col.b) * 200 / 51) << "\n";
                    //init_pair(ci, ci, 0);
                    //ci++;
                }
            }
        }
        //std::cout << "END COLS\n";
        
        for(unsigned int c2 = 1; c2 < ColorGradeInc; c2+=2) {
            for(unsigned int c1 = 0; c1 < ColorGradeInc; c1++) {
                int pi = (c2/2)*ColorGradeInc + c1;
                //Since we have to munch a bit off the 'b' component because some idiot forgot to type /unsigned/ short, make sure the noticable colors (black+white) are availible
                //This hack...I feel dirty
                init_extended_pair(pi, c1, c2 == 1? c2 = 0 : c2);
                //std::cerr << "init " << pi << " : " << c1 << ", " << c2 << "\n";
            }
        }
        
        wrefresh(stdscr);
        return true;
    }
    void CursesObject::terminate()  {
        if(!isendwin()) endwin();
    }
    
    void CursesObject::write(const uivec& pos, const std::string& str, const Style& style) {
        this->applyStyle(style);
        
        for(unsigned int i = 0; i < str.size(); i++) {
            mvwaddch(stdscr, int(pos.y), int(pos.x) + i, str[i]);
        }
    }
    
    void CursesObject::writew(const uivec& pos, const char *str, const Style& style) {
        this->applyStyle(style);
        mvwprintw(stdscr, int(pos.y), int(pos.x) , str);
        /*int i = 0;
        while(str != 0x0) {
            mvwaddch(stdscr, int(pos.y), int(pos.x) + i, *str);
            str++;
            i++;
        }*/
    }
    void CursesObject::writew(const uivec& pos, const wchar_t *str, const Style& style) {
        this->applyStyle(style);
        mvwprintw(stdscr, int(pos.y), int(pos.x) , reinterpret_cast<const char*>(str));
    }
    
    void CursesObject::applyStyle(const Style& style) {
        if(style.color.fg == style.color.bg) return;//Invalid style, skip
        
        auto pairID = this->getColorPair(style.color.fg, style.color.bg);
        auto styleFlags = this->getStyleFlags(style);
        attr_set(styleFlags, pairID, nullptr);
        wrefresh(stdscr);
    }
    
    void CursesObject::box(const uivec& ul, const uivec& lr, const Style& style) {
        this->applyStyle(style);
        uivec sz = lr - ul;
        //::wmove(stdscr, int(ul.y), int(ul.x));
        //::box(stdscr, int(size.y), int(size.x));
        this->line({ul.x, ul.y}, {ul.x, lr.y});
        this->line({ul.x, ul.y}, {lr.x, ul.y});
        this->line({lr.x, lr.y}, {ul.x, lr.y});
        this->line({lr.x, lr.y}, {lr.x, ul.y});
        
        mvaddch(int(ul.y), int(ul.x), ACS_ULCORNER);
        mvaddch(int(lr.y), int(ul.x), ACS_LLCORNER);
        mvaddch(int(ul.y), int(lr.x), ACS_URCORNER);
        mvaddch(int(lr.y), int(lr.x), ACS_LRCORNER);
    }
    void CursesObject::line(uivec s, uivec e, const Style& style) {
        this->applyStyle(style);
        if(s.x > e.x) {
            auto tmp = s.x;
            s.x = e.x;
            e.x = tmp;
        }
        if(s.y > e.y) {
            auto tmp = s.y;
            s.y = e.y;
            e.y = tmp;
        }
        
        
        uivec sz = e - s;
        if(sz.x == 0) {
            mvvline(int(s.y), int(s.x), ACS_VLINE, int(sz.y));
        } else if(sz.y == 0) {
            mvhline(int(s.y), int(s.x), ACS_HLINE, int(sz.x));
        } else {
            throw std::runtime_error("Non-straight line draw requested");
        }
    }
    
    void CursesObject::image(const uivec& ul, const uivec& sz, const Style& style) {
        
    }
    
    void CursesObject::entity(const uivec& pos, entity_t enty, const Style& style) {
        
    }
    
    void CursesObject::update() {
        int row, col;
        getmaxyx(stdscr,row,col);
        size.x = col;
        size.y = row;
        wrefresh(stdscr);
        
    }
    
    int CursesObject::getStyleFlags(const Style& style) {
        int ret = 0x0;
        if(style.is(Style::Highlight)) ret |= A_STANDOUT;
        if(style.is(Style::Bold)) ret |= A_BOLD;
        if(style.is(Style::Dim)) ret |= A_DIM;
        if(style.is(Style::Underline)) ret |= A_UNDERLINE;
        if(style.is(Style::Blink)) ret |= A_BLINK;
        
        return ret;
    }
    
    unsigned int CursesObject::getColor(ColorRGBA col) {
        int r, g, b, er, eg, eb;
        r = int(col.r);
        g = int(col.g);
        b = int(col.b);
        
        er = r * ColorGradeMult / 255;
        eg = g * ColorGradeMult / 255;
        eb = b * ColorGradeMult / 255;
        
        unsigned int colID = eb + eg*ColorGrades + er * (ColorGrades*ColorGrades) + 16;
        return colID;
    }
    
    unsigned CursesObject::getColorPair(ColorRGBA fg, ColorRGBA bg) {
        /*if(fg == cfg && bg == cbg) return 0;
        else */if(fg == bg) {
           return 0;
        }
        
        cfg = fg;
        cbg = bg;
        
        int pairID = this->getColor(fg) + (this->getColor(bg)/2)*ColorGradeInc;//\\\ + 16;
        //std::cout << "colorPair: " << pairID << " from cid = " << this->getColor(fg) << ", col = " << fg.toString() <<"\n";
        //if(pairID > 32768) pairID %= 32768;
        /*static int uidbgOff = 25;
        int dr = 0, db = 0, dg = 0, bbr = 0, bbb = 0, bbg = 0;
        int cp1 = 0, cp2 = 0;
        extended_pair_content(pairID, &cp1, &cp2);
        extended_color_content(cp1, &dr, &dg, &db);
        extended_color_content(cp2, &bbr, &bbg, &bbb);
        
        std::stringstream ss;
        ss << pairID << " : " << dr << ", " << dg << ", " << db << " | " << bbr << ", " << bbg << ", " << bbb << " reported, pair " << cp1 << ":" << cp2 <<", should be " << this->getColor(fg) << ":" << this->getColor(bg)<<"\n";
        
        
        
        //sleep(5);
        this->write({10,uidbgOff}, ss.str());
        uidbgOff++;*/
        //this->update();
        return pairID;
        
    }
}
