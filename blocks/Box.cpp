#include "Box.h"
#include "Context.h"

namespace arke::ui {
    void Box::draw() {
        switch(ActiveContext->type()) {
            default:
            case Context::TUI:
                ActiveContext->box(this->absolute(), this->absolute() + this->size, this->style);
                break;
        }
        
        Object::draw();
    }
}

