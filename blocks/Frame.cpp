#include "Frame.h"
#include "Context.h"

namespace arke::ui {
    void Frame::draw() {
        Box::draw();
        
        if(!title.empty()) {
            uivec titleOffset = {3,0};//{(this->size.x - uiunit(title.size()))/uiunit(2), 0};
            switch(ActiveContext->type()) {
                default:
                case Context::TUI:
                    ActiveContext->writew(this->absolute() + titleOffset, title.c_str(), this->style);
                    break;
            }
        }
        
    }
}
