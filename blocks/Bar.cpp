#include "Bar.h"
#include "Context.h"
#include <ncurses.h>

namespace arke::ui {
    void Bar::setProgress(float p) {
        progress = p;
        this->setDirty();
    }
    float Bar::getProgress() const {
        return progress;
    }
    
    void Bar::draw() {
        float drawWidth = double(this->size.x) * this->getProgress();
        uivec base = this->absolute();
        
        Style backStyle = this->style, frontStyle = this->style;
        //backStyle.color.bg = this->style.color.bg;
        frontStyle.color.bg = this->style.color.fg;
        frontStyle.color.fg = this->style.color.bg;
        
        char blockChar[] = {' '};
        
        switch(ActiveContext->type()) {
            default:
            case Context::TUI:
                //ActiveContext->box(this->absolute(), this->absolute() + this->size, this->style);
                for(int i = 0; i < int(this->size.x); i++) {
                    std::string chr = blockChar;
                    unsigned int rightThresh = int(this->size.x) - text.r.size();
                    
                    if(i < text.l.size()) {
                        chr = text.l[i];
                    } else if(i >= rightThresh) {
                        chr = text.r[i - rightThresh];
                    }
                    
                    if(i == int(::floor(drawWidth))) {
                        Style fadedStyle = this->style;
                        fadedStyle.color.bg.lerp(this->style.color.fg, 1.f - (drawWidth - ::floor(drawWidth)));
                        
                        ActiveContext->writew(base + uivec{i, 0}, chr.c_str(), fadedStyle);
                    } else if(i < ::floor(drawWidth)) {
                        ActiveContext->writew(base + uivec{i, 0}, chr.c_str(), frontStyle);
                    } else {
                        ActiveContext->writew(base + uivec{i, 0}, chr.c_str(), backStyle);
                    }
                }
                break;
        }
    }
    
    void Bar::setLeftText(std::string txt) {
        text.l = txt;
    }
    void Bar::setRightText(std::string txt) {
        text.r = txt;
    }
}
