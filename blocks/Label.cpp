#include "Label.h"
#include "Context.h"

namespace arke::ui {
    void Label::draw() {
        switch(ActiveContext->type()) {
            default:
            case Context::TUI:
                ActiveContext->writew(this->absolute(), this->getDataStr().c_str(), this->style);
                break;
        }
        
        Object::draw();
    }
    
    std::string Label::getDataStr() const {
        if(data.size() > this->size.x) {
            return data.substr(0, int(this->size.x));
        } else if(data.size() < this->size.x) {
            return data + std::string(int(this->size.x) - data.size(), ' ');
        } else return data;
    }
}
