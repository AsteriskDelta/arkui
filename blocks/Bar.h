#ifndef ARKUI_BAR_H
#define ARKUI_BAR_H
#include "Object.h"

namespace arke::ui {
    class Bar : public Object {
    public:
        
        virtual void setProgress(float p);
        virtual float getProgress() const;
        
        virtual void setLeftText(std::string txt);
        virtual void setRightText(std::string txt);
        
        virtual void draw() override;
    protected:
        float progress;
        struct {
            std::string l, r;
        } text;
    };
}

#endif
