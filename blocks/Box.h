#ifndef ARKUI_BOX_H
#define ARKUI_BOX_H
#include "Object.h"

namespace arke::ui {
    class Box : public Object {
    public:
        
        virtual void draw() override;
    protected:
    };
}

#endif
