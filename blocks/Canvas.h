#ifndef ARKUI_CANVAS_H
#define ARKUI_CANVAS_H
#include "Object.h"

namespace arke::ui {
    class Canvas : public Object {
    public:
        /*struct Char {
            char data[4];
            ui::Style style;
        };*/
        //std::vector<Char> data;
        //std::vector<uint16_t> delta;
        
        //virtual void resize(uivec sz) override;
        
        void write(uivec pos, std::string dat, const Style& st = Style());
        
        virtual void draw() override;
    protected:
        
    };
}

#endif
