#ifndef ARKUI_LABEL_H
#define ARKUI_LABEL_H
#include "Object.h"

namespace arke::ui {
    class Label : public Object {
    public:
        std::string data;
        
        virtual void draw() override;
    protected:
        std::string getDataStr() const;
    };
}

#endif
