#ifndef ARKUI_FRAME_H
#define ARKUI_FRAME_H
#include "Object.h"
#include "Box.h"

namespace arke::ui {
    class Frame : public Box {
    public:
        std::string title;
        
        virtual void draw() override;
    protected:
    };
}

#endif
