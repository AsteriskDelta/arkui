#include "Canvas.h"
#include "Context.h"

namespace arke::ui {
    void Canvas::write(uivec pos, std::string dat, const Style& st) {
        if(pos > this->size) return;
        ActiveContext->writew(pos + this->absolute(), dat.c_str(), st);
    }
}
