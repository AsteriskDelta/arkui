#include <NVX/NVX.h>
#include <Spinster/Spinster.h>
#include <ARKE/ARKE.h>
#include <ARKE/Proxy.h>

using namespace nvx;

typedef NI2<24,8,0> uiunit;
typedef NVX2<3, uiunit> uivec;

typedef NI2<8,24,0> uimval;
typedef NVX2<3, uimval> uimvec;

namespace arke {
    namespace ui {
        class Context;
        extern Context *ActiveContext;
    }
}
