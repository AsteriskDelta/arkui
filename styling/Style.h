#ifndef ARKUI_STYLE_H
#define ARKUI_STYLE_H
#include <ARKE/Color.h>

namespace arke::ui {
    struct Style {
        struct {
            ColorRGBA fg = ColorRGBA(), bg = ColorRGBA();
        } color;
        
        typedef uint32_t Flag;
        constexpr static const Flag
            Bold            = 0b1,
            Italic          = 0b10,
            Underline       = 0b100,
            Strikethrough   = 0b1000,
            Superscript     = 0b10000,
            Subscript       = 0b100000,
            Highlight       = 0b1000000,
            Dim             = 0b10000000,
            Blink           = 0b100000000;
        Flag flags = 0x0;
        inline bool is(const Flag& f) const {
            return (flags & f) == f;
        }
        inline void set(const Flag& f) {
            flags |= f;
        }
        inline void clear(const Flag& f) {
            flags &= ~f;
        }
    };
}

#endif
