#include "Product.h"
#include "Index.h"

namespace GDAX {
    IndexObject Index;
    
    Product* IndexObject::AddProduct(const std::string& abbrv, const std::string& name) {
        Product *const productPtr = new Product(name, abbrv);
        this->insert({abbrv, productPtr});
        return productPtr;
    }
    
    void IndexObject::addEntry(const StreamEntry& entry) {
        IndexHistoryNode::addEntry(entry);
        
        //Chose a half-gaussian distribution of points and insert sample them into the HDI
        const unsigned int HDISuperInsertCnt = 8;
        const unsigned int HDISampleRange = 3600 * 24 * 7;
        const unsigned short HDISampleTaps = 24;
        /*
        for(unsigned int i = 0; i < HDISuperInsertCnt; i++) {
            Time dTime = (rand()) % HDISampleRange;
            Time dTimeRadius, dTimeEpsilon;
            const Time smpIdealTime = entry.time - dTime;
            
            StreamEntry smp = this->sample(smpIdealTime, dTimeRadius, HDISampleTaps);
            if(fabs(smp.time - smpIdealTime) > dTimeEpsilon) continue;//Available data in this section isn't dense enough to be significant
            
            
        }*/
        
        /*static unsigned long long entryCount = 0;
        entryCount++;
        if(entryCount % 1000 == 0) {
            std::cout << "Index: loaded " << entryCount << " entries\n";
        }*/
    }
};
